package com.example.hong.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button add,sub,div,mul,btnSum,clear;

    EditText tv1,tv2,tv3,tvSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnOperation();
        sum();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public void btnOperation()
    {

        tv2=(EditText)findViewById(R.id.tvOpt);
       //btnAdd
        add=(Button)findViewById(R.id.btnAdd);add.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            //Toast t=Toast.makeText(getApplicationContext(),"hej hej",Toast.LENGTH_SHORT);
           // t.show();
            tv2.setText("+");
        }
        });
        //btnMinus
        sub=(Button)findViewById(R.id.btnSub);sub.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
             tv2.setText("-");
        }
         });
        //btnMul
        mul=(Button)findViewById(R.id.btnMul);mul.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            tv2.setText("*");
        }
    });
        //btndiv
        div=(Button)findViewById(R.id.btnDiv);div.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            tv2.setText("/");
        }
    });
    }
    public void sum()
    {
        tv1=(EditText)findViewById(R.id.NrOne);
        tv3=(EditText)findViewById(R.id.NrTwo);
        tvSum=(EditText)findViewById(R.id.tvResult);
        btnSum=(Button)findViewById(R.id.btnEqual);
        btnSum.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tv1.length()==0||tv3.length()==0||tv2.length()==0)//test tom
                {
                    //tv1.setText("mata in");
                    Toast t=Toast.makeText(getApplicationContext(),"Mata in tal eller operation",Toast.LENGTH_SHORT);
                     t.show();
                }
                else
                {  String nr1 = tv1.getText().toString();
                    String nr2=tv3.getText().toString();
                    try {
                        int num1 = Integer.parseInt(nr1);
                        int num2 = Integer.parseInt(nr2);
                        int total=num1+num2;
                        tvSum.setText(String.valueOf(total));
                    } catch (NumberFormatException e) {
                        Toast.makeText(getApplicationContext(),"Ej tal.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

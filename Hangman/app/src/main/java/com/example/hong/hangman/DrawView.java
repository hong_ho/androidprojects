package com.example.hong.hangman;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by Hong on 2015-09-08.
 */
public class DrawView extends View{
    private int antalFel=0;
    Paint p;
    int x=150;
    int y=50;

    public DrawView(Context context,int antalFel){
        super(context);
        this.antalFel=antalFel;
        p=new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.GREEN);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);




        switch (antalFel)
        {
            case 0: tree(canvas);break;
            case 1:
                tree(canvas);
                head(canvas);
                break;
            case 2:
                tree(canvas);
                head(canvas);
                leftHand(canvas);
                break;
            case 3:
                tree(canvas);
                head(canvas);
                leftHand(canvas);
                rightHand(canvas);
                break;
            case 4:
                tree(canvas);
                head(canvas);
                leftHand(canvas);
                rightHand(canvas);
                body(canvas);
                break;
            case 5:
                tree(canvas);
                head(canvas);
                leftHand(canvas);
                rightHand(canvas);
                body(canvas);
                leftLeg(canvas);
                break;
            case 6:
                tree(canvas);
                head(canvas);
                leftHand(canvas);
                rightHand(canvas);
                body(canvas);
                leftLeg(canvas);
                rightLeg(canvas);
                break;
            case 7:
                tree(canvas);
                head(canvas);
                leftHand(canvas);
                rightHand(canvas);
                body(canvas);
                leftLeg(canvas);
                rightLeg(canvas);
                line(canvas);
                break;

        }

    }
    public void tree(Canvas canvas)
    {
        canvas.drawLine(x, y, x, y + 500, p);
        canvas.drawLine(x, y, x + 300, y, p);
        canvas.drawLine(x, y + 50, x + 50, y, p);
        canvas.drawLine(x - 50, y + 500, x + 500, y + 500, p);
    }
    public void head(Canvas canvas)
    {
        canvas.drawCircle(x + 200, y + 100, 50, p);
    }
    public void body(Canvas canvas)
    {
        canvas.drawLine(x+200, y+150, x+200, y + 300, p);
    }
    public void leftHand(Canvas canvas)
    {
        canvas.drawLine(x+200,y+170,x+100,y+150,p);
    }
    public void rightHand(Canvas canvas)
    {
        canvas.drawLine(x+200,y+170,x+300,y+150,p);
    }
    public void leftLeg(Canvas canvas)
    {
        canvas.drawLine(x+200,y+300,x+100,y+350,p);
    }
    public void rightLeg(Canvas canvas)
    {
        canvas.drawLine(x+200,y+300,x+300,y+350,p);
    }
    public void line(Canvas canvas)
    {
        canvas.drawLine(x+200,y,x+200,y+150,p);
    }


}



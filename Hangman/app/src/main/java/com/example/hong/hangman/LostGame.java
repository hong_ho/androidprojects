package com.example.hong.hangman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Hong on 2015-09-14.
 */
public class LostGame extends Activity implements View.OnClickListener{
    Intent t;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lose);

        Button okBtn=(Button)findViewById(R.id.btnOkLost);
        okBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        t=new Intent(this,MainActivity.class);
        startActivity(t);

    }
}

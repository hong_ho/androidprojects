package com.example.hong.hangman;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,View.OnKeyListener{
    EditText userInput;
    Button tryBtn,newBtn;
    LinearLayout layoutWord,layoutDraw;
    public Intent t=null;

    private String[] words={"rose","dear","flower","love","winter","autum","spring","summer"};
    private String word;
    Random r;
    private int antalFel=0;
    int correct=0;
    EditText tvWord;
    List<EditText> arrEdit=new ArrayList<EditText>();

    String user;
    char c;
    DrawView dv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initial();
        startGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    public void initial()    {
        tryBtn=(Button)findViewById(R.id.btnTry);
        tryBtn.setOnClickListener(this);

        newBtn=(Button)findViewById(R.id.btnNew);
        newBtn.setOnClickListener(this);

        layoutWord=(LinearLayout)findViewById(R.id.containWord);
        layoutDraw=(LinearLayout)findViewById(R.id.containDraw);

        userInput =(EditText)findViewById(R.id.userText) ;
        userInput.setOnKeyListener(this);
        userInput.requestFocus();
    }

    public void startGame()
    {
        randomWord();
        createEditText();
    }
    public void createEditText()
    {
        for(int i=0;i<word.length();i++)
        {
            tvWord=new EditText(this);
            tvWord.setWidth(150);
            tvWord.setTextColor(Color.BLUE);
            tvWord.setEnabled(false);
            tvWord.setId(i);
            tvWord.setGravity(Gravity.CENTER);

            arrEdit.add(tvWord);
            layoutWord.addView(tvWord);
        }
    }
     public void randomWord()
     {
         r=new Random();
         word=words[r.nextInt(words.length)] ;
     }

     public void checkRight()
    {
        user=userInput.getText().toString();
        c=character(user);
        if(c==' ')
           Toast.makeText(getApplicationContext(),"Mat in en bokstav ",Toast.LENGTH_LONG).show();
        else if(user.length()>1)
        {
            Toast.makeText(getApplicationContext(),"Mat bara in en bokstav! ",Toast.LENGTH_LONG).show();
        }
        else
        {
            checkContains();
            checkWin();
        }
        userInput.setText("");
    }
    public char character(String user)
    {
        char n;
        if (user.length()!=0)
        {
            n=user.charAt(0);
        }
        else
        {
            n=' ';
        }
        return n;
    }
    public void checkContains()
    {
        if(word.contains(user))
        {
            for(int i =0; i < word.length();++i)
            {
                char ans = word.charAt(i);
                if(c == ans)
                {
                    if(arrEdit.get(i).getText().toString().length()!=0||!arrEdit.get(i).getText().toString().equals(""))
                        Toast.makeText(getApplication(),"redan finns",Toast.LENGTH_LONG).show();
                    else
                    {
                        arrEdit.set(i, tvWord).setText(String.valueOf(c));
                        correct++;
                    }
                }
            }
        }
        else
        {
            antalFel++;
            checkDie(antalFel);
        }
    }

    public void checkWin()
    {
        if(correct==word.length())
        {
            t=new Intent(this,WinGame.class);
            startActivity(t);
        }
    }
    public void drawDie(int antalFel)
    {
        dv=new DrawView(this,antalFel);
        layoutDraw.addView(dv);
    }
    public void checkDie(int antalFel)
    {
        switch (antalFel)
        {
            case 1:
               drawDie(1);
                break;
            case 2:
                layoutDraw.removeAllViews();
                drawDie(2);
                break;
            case 3:
                layoutDraw.removeAllViews();
                drawDie(3);
                break;
            case 4:
                layoutDraw.removeAllViews();
                drawDie(4);
                break;
            case 5:
                layoutDraw.removeAllViews();
                drawDie(5);
                break;
            case 6:
                layoutDraw.removeAllViews();
                drawDie(6);
                break;
            case 7:
                layoutDraw.removeAllViews();
                drawDie(7);
                t=new Intent(this,LostGame.class);
                startActivity(t);
                break;
        }
    }
    public void tryGame()
    {
        antalFel=0;
        correct=0;
        userInput.setText("");
        layoutWord.removeAllViews();
        layoutDraw.removeAllViews();
        arrEdit.clear();
        createEditText();
    }
    public void newGame()
    {
        antalFel=0;
        correct=0;
        arrEdit.clear();
        layoutWord.removeAllViews();
        layoutDraw.removeAllViews();
        startGame();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnTry:
                tryGame();
                break;
            case R.id.btnNew:
                newGame();
                break;
        }
    }
    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if(event.getAction()==KeyEvent.ACTION_DOWN && keyCode==KeyEvent.KEYCODE_ENTER)
        {
            checkRight();
            user="";
            return true;
        }
        return false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

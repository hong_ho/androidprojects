package com.example.hong.hangman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Hong on 2015-09-14.
 */
public class WinGame extends Activity implements View.OnClickListener{
    Intent intent;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.win);

        Button okBtn=(Button)findViewById(R.id.btnOK);
        okBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        intent=new Intent(this,MainActivity.class);
        startActivity(intent);

    }
}

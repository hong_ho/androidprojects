package com.example.hong.searchartistxml;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private ReadXML file;
    private List<String> lst;
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn=(Button)findViewById(R.id.button1);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText genre=(EditText)findViewById(R.id.autoCompleteTextView1);
                String gen=genre.getText().toString().replace(' ','+');
                MyAsynctask bg=new MyAsynctask(MainActivity.this);
                bg.execute(gen);
            }
        });
    }
    public void backgroundTaskFinished(List<String> l)
    {
        adapter=new ArrayAdapter(MainActivity.this,android.R.layout.simple_list_item_1,l);
        ListView lv=(ListView)findViewById(R.id.listView);
        lv.setAdapter(adapter);
    }

}

package com.example.hong.searchartistxml;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hong on 2015-09-29.
 */
public class MyAsynctask extends AsyncTask<String, Void, List<String>>{

    private MainActivity mainActivity;
    private ReadXML readXML;

    public  MyAsynctask(MainActivity mainActivity)
    {
        this.mainActivity=mainActivity;
    }

    //vad ska man göra i background
    @Override
    protected List<String> doInBackground(String... params) {
        List<String> lst=new ArrayList<String>();
        readXML=new ReadXML();
        lst=readXML.readData(params[0]);
        return lst; // return artistList
    }

    @Override
    protected void onPostExecute(List<String> strings) {
        //super.onPostExecute(strings);
        mainActivity.backgroundTaskFinished(strings);
    }
}

package com.example.hong.searchartistxml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hong on 2015-09-27.
 */
public class ReadXML {

    private List<String> list=new ArrayList<String>();
    private String id;
    public String id(String genre)
    {
        URL url;
        try
        {
            url=new URL("http://developer.echonest.com/api/v4/artist/search?api_key=ERNTC32MCUMWDQTFE&format=xml&name="+genre+"&results=1");

            XmlPullParserFactory parseCreate=XmlPullParserFactory.newInstance();
            XmlPullParser parse=parseCreate.newPullParser();
            parse.setInput(url.openStream(),null);

            int eventParse=parse.getEventType();
            String tagName;
            while (eventParse!=XmlPullParser.END_DOCUMENT)
            {
                switch (eventParse)
                {
                    case XmlPullParser.START_TAG:
                        tagName=parse.getName();
                        if (tagName.equals("id"))
                        {
                            id=parse.nextText();
                        }
                        break;
                }
                eventParse= parse.next();
            }
        }catch(MalformedURLException ue)
        {
            ue.printStackTrace();
        }catch (XmlPullParserException pe)
        {
            pe.printStackTrace();
        }catch (IOException ie)
        {
            ie.printStackTrace();
        }
        return id;
    }
    public List<String> readData(String genre)
    {
        String id=id(genre);
        URL url;
        try
        {
            url=new URL("http://developer.echonest.com/api/v4/artist/similar?api_key=ERNTC32MCUMWDQTFE&id="+id+"&format=xml&results=10&start=0");

            XmlPullParserFactory parseCreate=XmlPullParserFactory.newInstance();
            XmlPullParser parse=parseCreate.newPullParser();
            parse.setInput(url.openStream(),null);

            int eventParse=parse.getEventType();
            String tagName;
            while (eventParse!=XmlPullParser.END_DOCUMENT)
            {
                switch (eventParse)
                {
                    case XmlPullParser.START_TAG:
                        tagName=parse.getName();
                        if (tagName.equals("name"))
                        {
                             list.add(parse.nextText());
                        }
                        break;
                }
                eventParse= parse.next();
            }
        }catch(MalformedURLException ue)
        {
            ue.printStackTrace();
        }catch (XmlPullParserException pe)
        {
            pe.printStackTrace();
        }catch (IOException ie)
        {
            ie.printStackTrace();
        }
        return list;
    }
}

package com.example.hong.searchmoviesjson;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hong on 2015-10-07.
 */
public class ArtistAsynctask extends AsyncTask<String,Void,List<String>> {

    private SearchArtists artists;
    private ShortListActivity shortListActivity;

    public ArtistAsynctask(ShortListActivity shortListActivity)
    {
        this.shortListActivity=shortListActivity;
    }
    @Override
    protected List<String> doInBackground(String... params) {
        List<String> lst=new ArrayList<String>();
        artists=new SearchArtists();
        lst=artists.loadListArtists(params[0]);

        return lst;
    }
    @Override
    protected void onPostExecute(List<String> strings) {
        shortListActivity.searchArtist(strings);
    }
}

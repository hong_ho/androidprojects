package com.example.hong.searchmoviesjson;

import android.os.AsyncTask;

/**
 * Created by Hong on 2015-10-09.
 */
public class DeleteAsynctask extends AsyncTask<String,Void,Void> {
    private ShortListActivity shortListActivity;

    public DeleteAsynctask(ShortListActivity shortListActivity)
    {
        this.shortListActivity=shortListActivity;
    }
    @Override
    protected Void doInBackground(String... params) {
        ShortListMovie movieDb=new ShortListMovie(shortListActivity,null,null,1);
        movieDb.deleteMovie(params[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        shortListActivity.delFinished();
    }
}

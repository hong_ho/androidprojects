package com.example.hong.searchmoviesjson;

import android.database.Cursor;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hong on 2015-10-09.
 */
public class LoadUpMovieAsynctask extends AsyncTask<Void,Void,List>
{

    private ShortListActivity shortListActivity;

    public LoadUpMovieAsynctask(ShortListActivity shortListActivity)
    {
        this.shortListActivity=shortListActivity;
    }

    @Override
    protected List doInBackground(Void... params) {
        List l=new ArrayList();
        ShortListMovie movieDb=new ShortListMovie(shortListActivity,null,null,1);
        l=movieDb.showAllData();
        return l;
    }
    @Override
    protected void onPostExecute(List l) {
       shortListActivity.showData(l);
    }
}

package com.example.hong.searchmoviesjson;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener{

    private List<String> lst, lstArtists;
    private ArrayAdapter aa;
    private ReadMoviesFile file;
    private SearchArtists artists;
    private ListView lv;
    private EditText name;
    private Button btnSearch, btnSearchArtist;
    private Intent intent=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        name=(EditText)findViewById(R.id.autoCompleteTextView1);

        lv=(ListView)findViewById(R.id.listView1);
        lv.setOnItemClickListener(this);

        btnSearch=(Button)findViewById(R.id.button1);
        btnSearch.setOnClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String item=(String)parent.getItemAtPosition(position);
        String name=item.replace(' ', '+');
        //Toast.makeText(this,"hej"+name,Toast.LENGTH_SHORT).show();

        intent=new Intent(this,SimilarMoviesActivity.class);
        intent.putExtra("movie",name);
        startActivity(intent);
    }
    @Override
    public void onClick(View v) {

        if(TextUtils.isEmpty(name.getText().toString()))
        {
            name.setError("You did not enter a moviesname!");
        }
        else
        {
            String suggMovie=name.getText().toString().replace(' ','+');
            SuggMoviesAsynctask background=new SuggMoviesAsynctask(MainActivity.this);
            background.execute(suggMovie);
        }
    }
    public void searchSugMovies(List<String> listSuggMovies )
    {
        TextView tv=(TextView)findViewById(R.id.textView3);
        tv.setText("Click on item to have similar movies");
        aa=new ArrayAdapter(MainActivity.this,android.R.layout.simple_list_item_1,listSuggMovies);
        lv.setAdapter(aa);

    }
}

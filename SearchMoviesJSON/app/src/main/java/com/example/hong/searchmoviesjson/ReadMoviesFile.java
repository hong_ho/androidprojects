package com.example.hong.searchmoviesjson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Hong on 2015-10-03.
 */
public class ReadMoviesFile
{
    private List<String> list;

    public List<String> loadList(String name)
    {
        list=new ArrayList<String>();
        String text;

        text=readJsonFile(name);
        try
        {
            JSONObject obj=new JSONObject(text);
            JSONArray array=obj.getJSONArray("results");
           for(int i=0;i<array.length();i++)
           {
                JSONObject jsObj=array.getJSONObject(i);
                list.add(jsObj.getString("title"));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    private String readJsonFile(String name) {
        StringBuilder builder=new StringBuilder();
        HttpClient client=new DefaultHttpClient();

        HttpGet httpGet=new HttpGet("http://www.myapifilms.com/tmdb/searchMovie?movieName="+name+"&format=json");
        try
        {
            HttpResponse response=client.execute(httpGet);
            StatusLine statusLine=response.getStatusLine();
            int statusCode=statusLine.getStatusCode();

            if(statusCode==200)
            {
                HttpEntity entity=response.getEntity();
                InputStream content=entity.getContent();
                BufferedReader reader=new BufferedReader(new InputStreamReader(content));
                String line;
                while((line=reader.readLine())!=null)
                {
                    builder.append(line);
                }
            }
        }catch (ClientProtocolException e)
        {
            e.printStackTrace();
        }catch (IOException io)
        {
            io.printStackTrace();
        }
        return builder.toString();
    }

}

package com.example.hong.searchmoviesjson;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hong on 2015-10-04.
 */
public class ReadSimilarMovies {

    public List<String> loadListSim(String name)
    {
        List<String> lstSimilar;
        lstSimilar=new ArrayList<String>();
        String text;

        text=readSimilarMoviesFile(name);

        try
        {
            JSONArray all=new JSONArray(text);
            JSONObject obj=all.getJSONObject(0);
            JSONArray array=obj.getJSONArray("similarMovies");

            for(int i=0;i<array.length();i++)            {
                JSONObject jsObj=array.getJSONObject(i);
                lstSimilar.add(jsObj.getString("name"));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return lstSimilar;
    }
    private String readSimilarMoviesFile(String name) {
        StringBuilder builder=new StringBuilder();
        HttpClient client=new DefaultHttpClient();

        HttpGet httpGet=new HttpGet("http://www.myapifilms.com/imdb?title="+name+"&format=JSON&similarMovies=1&adultSearch=0");
        try
        {
            HttpResponse response=client.execute(httpGet);
            StatusLine statusLine=response.getStatusLine();
            int statusCode=statusLine.getStatusCode();

            if(statusCode==200)
            {
                HttpEntity entity=response.getEntity();
                InputStream content=entity.getContent();
                BufferedReader reader=new BufferedReader(new InputStreamReader(content));
                String line;
                while((line=reader.readLine())!=null)
                {
                    builder.append(line);
                }
            }
        }catch (ClientProtocolException e)
        {
            e.printStackTrace();
        }catch (IOException io)
        {
            io.printStackTrace();
        }
        return builder.toString();
    }

}

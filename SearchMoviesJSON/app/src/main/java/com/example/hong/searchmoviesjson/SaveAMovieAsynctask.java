package com.example.hong.searchmoviesjson;

import android.os.AsyncTask;


/**
 * Created by Hong on 2015-10-07.
 */
public class SaveAMovieAsynctask extends AsyncTask<String,Void,Void>
{
    private String fileName="listmovies.txt";
    private SimilarMoviesActivity similarMoviesActivity;

    public SaveAMovieAsynctask(SimilarMoviesActivity similarMoviesActivity)
    {
        this.similarMoviesActivity=similarMoviesActivity;
    }

    @Override
    protected Void doInBackground(String... params) {

        ShortListMovie movieDb=new ShortListMovie(similarMoviesActivity,null,null,1);
        movieDb.addMovie(params[0]);

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        similarMoviesActivity.saveFinish();
    }

}

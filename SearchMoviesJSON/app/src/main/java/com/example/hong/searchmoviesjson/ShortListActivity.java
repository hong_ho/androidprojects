package com.example.hong.searchmoviesjson;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Iterator;
import java.util.List;

public class ShortListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener {

    private Intent intent=null;
    private ListView lvlist;
    private Spinner sp;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_short_list);

        Button btnSearchArtist=(Button)findViewById(R.id.btnArtist);
        btnSearchArtist.setOnClickListener(this);
        Button btnSugg=(Button)findViewById(R.id.btnSugg);
        btnSugg.setOnClickListener(this);

        lvlist=(ListView)findViewById(R.id.lvShortList);
        lvlist.setOnItemClickListener(this);

        sp=(Spinner)findViewById(R.id.spinner);
       loadData();

    }
    public void loadData()
    {
        LoadUpMovieAsynctask data=new LoadUpMovieAsynctask(ShortListActivity.this);
        data.execute();
    }

    public void showData(List l)
    {
        ArrayAdapter aa=new ArrayAdapter(this,android.R.layout.simple_list_item_1,l);
        lvlist.setAdapter(aa);
        sp.setAdapter(aa);
    }

    public void searchArtist(List<String> listArtists)
    {
        ListView lst=(ListView)dialog.findViewById(R.id.listView);
        ArrayAdapter ab=new ArrayAdapter(ShortListActivity.this,android.R.layout.simple_list_item_1,listArtists);
        lst.setAdapter(ab);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnArtist:
                    String artist=String.valueOf(sp.getSelectedItem()).replace(' ','+');
                    ArtistAsynctask bg=new ArtistAsynctask(ShortListActivity.this);
                    bg.execute(artist);

                    dialog=new Dialog(this);
                    dialog.setContentView(R.layout.artistlist);
                    dialog.setTitle("Artists in the film: " + String.valueOf(sp.getSelectedItem()));
                    Button ok=(Button)dialog.findViewById(R.id.btnOK);
                    ok.setOnClickListener(this);
                    dialog.show();
                break;
            case R.id.btnSugg:
                intent=new Intent(this,MainActivity.class);
                startActivity(intent);
                break;
            case R.id.btnOK:
                dialog.dismiss();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String movie=(String)parent.getItemAtPosition(position);

        DeleteAsynctask delMovie= new DeleteAsynctask(ShortListActivity.this);
        delMovie.execute(movie);
    }
    public void delFinished()
    {
        loadData();
        Toast.makeText(this,"Delete the movie is finished...",Toast.LENGTH_SHORT).show();
    }
}

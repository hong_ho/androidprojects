package com.example.hong.searchmoviesjson;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hong on 2015-10-08.
 */
public class ShortListMovie extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="movieDB.db";
    private static final String TABLE_PRODUCT="movies";

    private static final String COLUMN_PNAME="moviename";


    public ShortListMovie(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRODUCTS_TABLE="CREATE TABLE "+TABLE_PRODUCT+" ("+COLUMN_PNAME+" TEXT);";
        db.execSQL(CREATE_PRODUCTS_TABLE);
        Log.i("DB", "Cretead" + CREATE_PRODUCTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS"+TABLE_PRODUCT);
        onCreate(db);
    }
    public void addMovie(String name)
    {
        ContentValues values=new ContentValues();
        values.put(COLUMN_PNAME,name);

        SQLiteDatabase db=this.getWritableDatabase();

        db.insert(TABLE_PRODUCT, null, values);
        db.close();
    }

    public Cursor showData()
    {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor todoCursor = db.rawQuery("SELECT  * FROM " + TABLE_PRODUCT, null);
        return todoCursor;
    }
    public List showAllData()
    {
        List list=new ArrayList();
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor todoCursor = db.rawQuery("SELECT  * FROM "+TABLE_PRODUCT, null);
        if (todoCursor.moveToFirst()) {
            do {
                list.add(todoCursor.getString(0));
            } while (todoCursor.moveToNext());
        }
        if (todoCursor != null && !todoCursor.isClosed()) {
            todoCursor.close();
        }
        return list;
    }

    public void deleteMovie(String movie)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        String query = "DELETE FROM " +TABLE_PRODUCT+ " WHERE "  + COLUMN_PNAME+ " = " + "'"+movie +"'" ;
        db.execSQL(query);
        db.close();
    }
}

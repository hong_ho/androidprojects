package com.example.hong.searchmoviesjson;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SimilarMoviesActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener {
    private Intent intent=null;
    private String name;
    private ReadSimilarMovies simMov;
    private ListView lvSim;
    private ArrayAdapter ab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_similar_movies);

        Intent intent=getIntent();
        name=intent.getStringExtra("movie").replace(' ', '+');

        lvSim=(ListView)findViewById(R.id.listViewSim);
        lvSim.setOnItemClickListener(this);

        SimilarMoviesAsynctask similarMoviesAsynctask=new SimilarMoviesAsynctask(SimilarMoviesActivity.this);
        similarMoviesAsynctask.execute(name);

        Button okSim=(Button)findViewById(R.id.btnOKSim);
        okSim.setOnClickListener(this);

        Button showList=(Button)findViewById(R.id.btnShow);
        showList.setOnClickListener(this);
    }
    public void showListArtists(List<String> lstArtist)
    {
        ab=new ArrayAdapter(SimilarMoviesActivity.this,android.R.layout.simple_list_item_1,lstArtist);
        lvSim.setAdapter(ab);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String movie=(String)parent.getItemAtPosition(position);//getItemAtPosition(position);

        SaveAMovieAsynctask save=new SaveAMovieAsynctask(SimilarMoviesActivity.this);
        save.execute(movie);
    }
    public void saveFinish()
    {
        Toast.makeText(this, "saving is finished...",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnOKSim:
                 intent=new Intent(this,MainActivity.class);
                 startActivity(intent);
                break;
            case R.id.btnShow:
                intent=new Intent(this,ShortListActivity.class);
                startActivity(intent);
                break;
        }
    }
}

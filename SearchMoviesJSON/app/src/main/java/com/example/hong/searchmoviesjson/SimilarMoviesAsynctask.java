package com.example.hong.searchmoviesjson;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hong on 2015-10-07.
 */
public class SimilarMoviesAsynctask extends AsyncTask<String, Void, List<String>> {


    ReadSimilarMovies similarMovies;
    SimilarMoviesActivity similarMoviesActivity;

    public SimilarMoviesAsynctask(SimilarMoviesActivity similarMoviesActivity)
    {
        this.similarMoviesActivity=similarMoviesActivity;
    }

    @Override
    protected List<String> doInBackground(String... params) {
        List<String> lst=new ArrayList<String>();
        similarMovies=new ReadSimilarMovies();
        lst=similarMovies.loadListSim(params[0]);
        return lst;
    }
    @Override
    protected void onPostExecute(List<String> result) {
        similarMoviesActivity.showListArtists(result);
    }
}

package com.example.hong.searchmoviesjson;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hong on 2015-10-06.
 */
public class SuggMoviesAsynctask extends AsyncTask<String, Void, List<String>> {

    private MainActivity mainActivity;
    private ReadMoviesFile suggMovies;

    public SuggMoviesAsynctask(MainActivity mainActivity)
    {
        this.mainActivity=mainActivity;
    }

    @Override
    protected List<String> doInBackground(String... params)
    {
        List<String> lst=new ArrayList<String>();
        suggMovies=new ReadMoviesFile();
        lst=suggMovies.loadList(params[0]);
        return lst;
    }
    @Override
    protected void onPostExecute(List<String> result) {      
        mainActivity.searchSugMovies(result);

    }
}

package com.example.termin3labb3;

//import com.example.termin3_labb3.R;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity implements Observer, android.view.View.OnClickListener
{
	private Button boardButton[];
	private TicTacToeModel M;
	private TicTacToeController C;
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        M=new TicTacToeModel();
        C=new TicTacToeController(M, this);
        M.addObserver(this);
        
        setContentView(R.layout.fragment_main);

     
        Button b =(Button)findViewById(R.id.startbutton);
      	b.setOnClickListener(C);
      	
    }

	public void startGame()
	{ // m�ste g�ra allt som ligger i gameboard  h�r 
		  setContentView(R.layout.gameboard);
		  
	    // skapa button
	    boardButton=new Button[M.getPlayBoard().length];
	    boardButton[0]=(Button)findViewById(R.id.one);
	    boardButton[1]=(Button)findViewById(R.id.two);
	    boardButton[2]=(Button)findViewById(R.id.three);
	    boardButton[3]=(Button)findViewById(R.id.four);
	    boardButton[4]=(Button)findViewById(R.id.five);
	    boardButton[5]=(Button)findViewById(R.id.six);
	    boardButton[6]=(Button)findViewById(R.id.seven);
	    boardButton[7]=(Button)findViewById(R.id.eight);
	    boardButton[8]=(Button)findViewById(R.id.nine);
	    
	    for(int i = 0; i < 9; i++)
	    {
	    	boardButton[i].setText("");
	    	boardButton[i].setOnClickListener(C);
	    	boardButton[i].setTag(i);
	    }
	    
	    
	    Button rB =(Button)findViewById(R.id.resetbutton);
      	rB.setOnClickListener(C);
      	Button eB =(Button)findViewById(R.id.exitbutton);
      	eB.setOnClickListener(C);
      	
      	TextView msg=(TextView)findViewById(R.id.tv_playx);
      	msg.setText("X turn");
	    
	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onClick(View v) 
	{
	}

	@Override
	public void update(Observable observable, Object data)
	{
		TicTacToeModel m=(TicTacToeModel)observable;
		TextView msg=(TextView)findViewById(R.id.tv_playx);
			
		if(m.getPlayer()==1)
			msg.setText("O turn");
		else
			msg.setText("X turn");
		for ( int i = 0; i < 9; i ++ )
		{
			if (!(m.getPlayBoard()[i] == 0) )
			{
				if ( m.getPlayBoard()[i] == 1 )
				{						
					this.boardButton[i].setText("X");
					this.boardButton[i].setTextColor(Color.GREEN);						
				}
				else
				{						
					this.boardButton[i].setText("O");
					this.boardButton[i].setTextColor(Color.RED);					
				}				
			}
		}
		if(m.checkForWinner(m.getPlayer())>0)
		{
			if(m.getPlayer()==1)
				{	
					msg.setText(m.getName(1) + " vinner");
					for ( int i = 0; i < 9; i ++ )
						this.boardButton[i].setEnabled(false);
				}			
			else
			{						
				msg.setText(m.getName(2)+" vinner"); 
				for ( int i = 0; i < 9; i ++ )
					this.boardButton[i].setEnabled(false);
				
			}
		}
		else if(m.checkFull())
			{
				msg.setText("Tyv�rr, spelplanen �r uppfylld, ingen vinner"); // full
			}
		if(!m.isPlaying())
		{
			  for(int i = 0; i < 9; i++)
			    {
			    	this.boardButton[i].setText(""); 
			    	this.boardButton[i].setEnabled(true);
			    }			
			  setContentView(R.layout.fragment_main);
		      Button b =(Button)findViewById(R.id.startbutton);
		      b.setOnClickListener(C);
			  m.setPlaying(true);			  
		}			
	}
}



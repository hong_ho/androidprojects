package com.example.termin3labb3;


import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TicTacToeController implements OnClickListener
{
	private TicTacToeModel M;
	private MainActivity t;
	
	TicTacToeController(TicTacToeModel m, MainActivity tm )
	{
		this.M=m;	
		this.t=tm;
	}

	public void exit()
	{
		M.exit();
	}

	@Override
	public void onClick(View arg0) 
	{
		if(arg0.getId() == R.id.startbutton)
		{
			EditText p1=(EditText)t.findViewById(R.id.tf_playx);
			EditText p2=(EditText)t.findViewById(R.id.tf_playo);
			
			String spl1=p1.getText().toString();
			String spl2=p2.getText().toString();
			
			M.setName(spl1, spl2);
			t.startGame();
			//M.newGame();			
		}
		else if(arg0.getId() == R.id.exitbutton)
		{
			M.exit();
		}
		else if(arg0.getId() == R.id.resetbutton)
		{
			M.reset();
		}
		else
		{
			//Log.d("Hong", "Ho" + arg0.getTag());
			M.playGame((Integer)arg0.getTag());
		}
		// TODO Auto-generated method stub
		//Log.d("OTHER", "Controller.onClick");
	}

}

package com.example.termin3labb3;

import java.util.Observable;

public class TicTacToeModel extends Observable
{	private int[] playBoard;
	private int player=1;
	private boolean playing=true;
	private int win=0;	
	private String playerName1, playerName2;
	//MainActivity MA;
	
	public TicTacToeModel()//MainActivity ma)
	{
		//MA=ma;		
		playBoard=new int[9];
		this.player=1;
		this.newGame();
	}
	
	
	public void setPlayer(int player)
	{		this.player=player;	}
	public int getPlayer()
	{		return this.player;	}
	public void setName(String p1, String p2)
	{
		 playerName1=p1;
		  playerName2=p2;
			
	}
	public String getName(int i)
	{
		if(i==1)
			return playerName1;
		else
			return playerName2;
	}
	public int[] getPlayBoard()
	{		return playBoard;	}
	public void setPlayBoard(int[] gb)
	{		playBoard=gb;	}
	public void setPlaying(boolean p)
	{		this.playing=p;	}
	public boolean isPlaying()
	{ 		return playing;	}
	public void checkTurn()
	{
		if(this.player==1)
			this.player=2;
		else
			this.player=1;
	}
	/*public void clearPlayBoard()
	{
		for(int i=0;i<playBoard.length;i++)
			playBoard[i]=0;
	}*/
	public void newGame()
	{
		for(int i=0; i<playBoard.length;i++)
			this.playBoard[i]=0;
	}
	public int checkForWinner(int player)
	{
		if((playBoard[0]==player && playBoard[1]==player && playBoard[2]==player)||					
			(playBoard[3]==player && playBoard[4]==player && playBoard[5]==player)||					
			(playBoard[6]==player && playBoard[7]==player && playBoard[8]==player)||					
			(playBoard[0]==player && playBoard[3]==player && playBoard[6]==player)||					
			(playBoard[1]==player && playBoard[4]==player && playBoard[7]==player)||					
			(playBoard[2]==player && playBoard[5]==player && playBoard[8]==player)||					
			(playBoard[0]==player && playBoard[4]==player && playBoard[8]==player)||					
			(playBoard[6]==player && playBoard[4]==player && playBoard[2]==player))							
				{win=player;}
		return win;
	}
	public boolean checkFull()
	{
		for(int i=0; i<playBoard.length;i++)
			if(playBoard[i]==0)
				return false;
		return true;
	}
	 public void playGame(int pos)
	 {
		 if(this.playBoard[pos]==0)
		 {
			 playing=true;
			 this.playBoard[pos]=this.getPlayer();
			 this.checkForWinner(this.getPlayer());
			 checkFull();
			 setChanged();
			 notifyObservers();
			 checkTurn();
		 }
		 
	 }
	 public void reset()
	 {
		 playing=false;
		 for(int i=0;i<playBoard.length;i++)
			 playBoard[i]=0;
		checkForWinner(0);
		this.player=1;
		setChanged();
		notifyObservers();
	 }
	public void exit()
	{
		playing=false;
		System.exit(0);
	}

	
		
	}	
	
	